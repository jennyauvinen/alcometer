import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  private genders = [];
  private times = [];
  private bottle = [];
  private weight: number;
  private gender: string;
  private time: number;
  private bottles: number;
  private litres: number;
  private grams: number;
  private burning: number;
  private promilles: any;
  private grams_left: number;

  constructor() { }

  ngOnInit() {

    this.genders.push('Male');
    this.genders.push('Female');

    this.times.push('1');
    this.times.push('2');
    this.times.push('3');
    this.times.push('4');
    this.times.push('5');

    this.bottle.push('1');
    this.bottle.push('2');
    this.bottle.push('3');
    this.bottle.push('4');
    this.bottle.push('5');

  }

  private calculate() {

    this.litres = this.bottles * 0.33;
    this.grams = this.litres * 8 * 4.5;

    this.burning = this.weight / 10;

    this.grams_left = this.grams - (this.burning * this.time);


    if (this.gender == 'Male') {
      this.promilles = (this.grams_left / (this.weight * 0.7));
    }
    else {
      this.promilles = (this.grams_left / (this.weight * 0.6));
    }
  }
}
